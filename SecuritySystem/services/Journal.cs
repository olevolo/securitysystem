﻿using SecuritySystem.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.services
{
    class Journal
    {
        static private Journal journal = new Journal();

        public Queue<Log> queue;

        private Journal()
        {
            queue = new Queue<Log>();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Log(Log log)
        {
            queue.Enqueue(log);
            Console.WriteLine(log.ToString());
        }

        static public Journal getInstance()
        {
            return journal;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Log> lastActivator()
        {
            List<Log> logs = new List<Log>();
            Log tmp = this.queue.LastOrDefault(item => item.LoggerType == LoggerType.ACTIVATOR);
            Log tmp2;
            if(tmp != null)
            logs.Add(tmp);
            if (this.queue.Count > 5)
            {
                if (this.queue.ElementAt(this.queue.Count - 2).LoggerType == LoggerType.ACTIVATOR)
                {
                    tmp2 = this.queue.ElementAt(this.queue.Count - 2);
                    logs.Add(tmp2);
                }
            }

            //return this.queue.LastOrDefault(item => item.LoggerType == LoggerType.ACTIVATOR);
            return logs;
            //if (queue.Count > 0 && queue.First().LoggerType == LoggerType.ACTIVATOR)
            //{

            //    List<Log> list = new List<Log>();
            //    list.Add(queue.Dequeue());
            //    while (queue.First().LoggerType == LoggerType.ACTIVATOR)
            //    {
            //        list.Add(queue.Dequeue());
            //    }
            //    return list;
                
            //}
            //return new List<Log>();
        }
    }
}

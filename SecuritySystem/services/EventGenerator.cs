﻿using SecuritySystem.models;
using SecuritySystem.models.building;
using SecuritySystem.models.sensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SecuritySystem.services
{
    public class EventGenerator
    {
        private EventPublisher eventPublisher;
        private Building building;
        private static EventGenerator eventGenerator;

        public static EventGenerator getInstance() {
            if (eventGenerator == null)
                eventGenerator = new EventGenerator();
            return eventGenerator;
        }

        private EventGenerator() {}

        public void SetProperties(Building building) {
            this.building = building;

            List<Sensor> sensors = new List<Sensor>();
            foreach (Floor floor in building.Floors) {
                foreach (Room room in floor.Rooms) {
                    sensors.AddRange(room.Sensors.Values.SelectMany(i=>i));
                }
            }
            eventPublisher = new EventPublisher(sensors);
        }
        
        public void Run() {
            Thread thread1 = new Thread(DoWork);
            //Thread thread2 = new Thread(DoWork);
            thread1.Start();
            //thread2.Start();
        }

        public Random rnd;
        private void DoWork() {
            while (true) {
                Thread.Sleep(3000); //grow interval 
                int rndEvNum = new Random().Next(0, 4);
                EventType rndEvent = EventType.JEHOVAH_WITNESSES;
                switch (rndEvNum)
                {
                    case 0:
                        rndEvent = EventType.FIRE;
                        break;
                    case 1:
                        rndEvent = EventType.INVASION;
                        break;
                    case 2:
                        rndEvent = EventType.FLOOD;
                        break;
                    case 3:
                        rndEvent = EventType.JEHOVAH_WITNESSES;
                        break;
                }

                rnd = new Random();
                long rndFloor = rnd.Next(building.Floors.Count);
                long rndRoom = rnd.Next(building.Floors[(int)rndFloor].Rooms.Count);
                var sensorList = building.Floors[(int)rndFloor].Rooms[(int)rndRoom].Sensors.Values.SelectMany(i => i).Where(s => s.GetEventType() == rndEvent).Select(s => s.Id).ToList();
                 
                long rndSensorId = sensorList[rnd.Next(0, sensorList.Count)];
                long rndAnotherSensorId = sensorList[rnd.Next(0, sensorList.Count)];
                State curState = new State(building.Id, rndFloor, rndRoom);
                Event ev = new Event(curState, rndEvent, new DateTime().ToString(), rndSensorId);
                //Event ev2 = new Event(curState, rndEvent, new DateTime().ToString(), rndAnotherSensorId); 
                Console.WriteLine(Thread.CurrentThread.Name + ": " + ev);

                eventPublisher.notifySensors(ev);
                //eventPublisher.notifySensors(ev2);
            }
            
        }

        public string Builing { get; set; }
        public EventPublisher EventPublisher { get; set; }
    }
}

﻿using SecuritySystem.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.services.listeners
{
    class FloodEventListener : EventListener
    {
        public FloodEventListener(long id) : base(id)
        {
            evType = new EventType[1] { EventType.FLOOD };
            respType = ResponseType.LOWER_THE_WATER;
        }
    }
}

﻿using SecuritySystem.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.services.listeners
{
    class FireEventListener : EventListener
    {
        public FireEventListener(long id) : base(id)
        {
            evType = new EventType[1] { EventType.FIRE };
            respType = ResponseType.FIREFIGHTERS;
        }
    }
}

﻿using SecuritySystem.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.services.listeners
{
    class JehovaWitnessessEventListener : EventListener
    {
        public JehovaWitnessessEventListener(long id) : base(id)
        {
            evType = new EventType[1] { EventType.JEHOVAH_WITNESSES };
            respType = ResponseType.GO_TO_HELL;
        }
    }
}

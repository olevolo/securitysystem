﻿using SecuritySystem.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SecuritySystem.services.listeners
{
    public class InvasionEventListener : EventListener
    {

        public InvasionEventListener(long id) : base(id)
        {
            evType = new EventType[2] { EventType.INVASION, EventType.JEHOVAH_WITNESSES };
            respType = ResponseType.POLICE;
        }

    }
}

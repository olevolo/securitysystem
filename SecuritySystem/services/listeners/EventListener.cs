﻿using SecuritySystem.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SecuritySystem.services.listeners
{
    public abstract class EventListener
    {
        public long Id { get; set; }
        protected Dispatcher dispatcher;
        protected ResponseType respType;
        protected EventType[] evType;

        protected EventListener(long id)
        {
            Id = id;
            dispatcher = Dispatcher.getInstance();
            Listen();
        }

        public void Listen()
        {
            Thread thread1 = new Thread(Action);
            thread1.Start();

        }

        private void Action()
        {
            while (true)
            {
                while (!dispatcher.ContainsElements())
                {
                    //Thread.Sleep(3000);
                    //continue;
                }

                Message cur = dispatcher.getFirstMessage();

                if (evType.Contains(cur.Type))
                {
                   Journal.getInstance().Log(new Log(Id, cur.State, LoggerType.ACTIVATOR, true, cur.Type, respType));    
                }
                dispatcher.EraseQueue();

               

                Thread.Sleep(3000);
            }
        }

    }
}

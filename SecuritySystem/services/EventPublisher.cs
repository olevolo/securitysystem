﻿using SecuritySystem.models;
using SecuritySystem.models.sensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.services
{
    public class EventPublisher
    {
        private List<Sensor> sensors;

        public EventPublisher(List<Sensor> sensors)
        {
            this.sensors = sensors;
        }

        public List<Sensor> Sensors { get; set; }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void notifySensors(Event _event) {
            foreach (Sensor s in sensors)
            {
                s.notify(_event);
            }
        }

        public void remove(Sensor sensor)
        {
            sensors.Remove(sensor);
        }

        public void subscribeSensor(Sensor sensor)
        {
            sensors.Add(sensor);
        }

    }
}

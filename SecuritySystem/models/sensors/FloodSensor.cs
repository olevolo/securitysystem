﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models.sensors
{
    public class FloodSensor : Sensor
    {

        public FloodSensor(long id, State state) : base(id, state)
        {
            evType = EventType.FLOOD;
        }

     
    }
}

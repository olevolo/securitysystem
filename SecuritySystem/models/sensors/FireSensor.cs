﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models.sensors
{
    public class FireSensor : Sensor
    {
        
        public FireSensor(long id, State state) : base(id, state)
        {
            evType = EventType.FIRE;
        }

    }
}

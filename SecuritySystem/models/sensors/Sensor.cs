﻿using SecuritySystem.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecuritySystem.models;
using System.Threading;

namespace SecuritySystem.models.sensors
{
    public abstract class Sensor
    {
        public long Id { get; set; }
        public State State { get; set; }
        public bool Active { get; set; }
        protected EventType evType;

        protected static Dispatcher dispatcher;

        protected Sensor(long id, State state)
        {
            Id = id;
            this.State = state;
            Active = true;
            dispatcher = Dispatcher.getInstance();
        }


        public void notify(Event _event)
        {

            if (evType.Equals(_event.Type) && _event.State.Equals(State) && _event.SensorId == Id)
            {
                Message message = new Message(_event.State, _event.Type, _event.Time);
                Journal.getInstance().Log(new Log(Id, State, LoggerType.SENSOR, Active, _event.Type, ResponseType.NOTHING));

                if (Active)
                {
                    dispatcher.dispatchMessage(message);
                }

                this.Active = false;

                new Thread(() => {
                    Thread.Sleep(3000);
                    this.Active = true;
                }).Start();
            }
            
        }

        public EventType GetEventType() { return evType; }
        

        public override string ToString()
        {
            return "Sensor " +Id + State.BuildingId + State.FloorId + State.RoomId;
        }
    }
}

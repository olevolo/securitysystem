﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public enum ResponseType
    {
        NOTHING,
        POLICE,
        FIREFIGHTERS,
        LOWER_THE_WATER,
        GO_TO_HELL
    }
}

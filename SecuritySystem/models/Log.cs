﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    class Log
    {
        public long Id { get; set; }
        public State State { get; set; }
        public LoggerType LoggerType { get; set; }
        public bool Active { get; set; }
        public EventType EvType { get; set; }
        public ResponseType RespType { get; set; }

        public Log(long id, State state, LoggerType loggerType, bool active, EventType evType, ResponseType respType)
        {
            Id = id;
            State = state;
            LoggerType = loggerType;
            Active = active;
            EvType = evType;
            RespType = respType;
        }

        public override string ToString()
        {
            return "Id: " + Id + " | State: {" + State + "} \n| Logger type:" + LoggerType
                + " | Active: " + Active + " | Event Type: " + EvType
                + " | Response Type:" + RespType;
        }
        public string ForTextBox()
        {
            return "Id: " + Id + Environment.NewLine + State.ForTextBox() + Environment.NewLine + "Type:" + LoggerType
               + Environment.NewLine + "Event Type: " + EvType + Environment.NewLine
                + "Response Type:" + RespType;
        }
    }
}

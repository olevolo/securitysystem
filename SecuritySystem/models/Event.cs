﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class Event
    {
        public State State { get; set; }
        public EventType Type { get; set; }
        public string Time { get; set; }
        public long SensorId { get; set; }

        public Event(State state, EventType type, string time, long sensorId)
        {
            State = state;
            Type = type;
            Time = time;
            SensorId = sensorId;
        }

        public override string ToString()
        {
            return State.ToString() +" |type:" + Type + "|time: " + Time + " |Sensor id: " + SensorId;
        }
    }
}

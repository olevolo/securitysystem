﻿using SecuritySystem.models.sensors;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Room : ISensorOwner
    {
        public long Id { get; set; }

        public int SafetyLevel { get; set; }

        public bool IsSecured { get; set; }

        public List<Hole> Holes;

        public double Square { get; set; }

        public Dictionary<ISensorOwner, List<Sensor>> Sensors { get; set; }

        public string Type { get; set; }

        public Room() { }

        public Room(long id, int safetyLevel, List<Hole> holes, double square, string type)
        {
            Id = id;
            SafetyLevel = safetyLevel;
            Holes = holes;
            Square = square;
            Type = type;
        }

        public Room (long id, int safetyLevel, List<Hole> holes, double square, Dictionary<ISensorOwner, List<Sensor>> sensors, string type)
        {
            Id = id;
            SafetyLevel = safetyLevel;
            Holes = holes;
            Square = square;
            Sensors = sensors;
            Type = type;
        }
    }
}

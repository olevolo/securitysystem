﻿using SecuritySystem.models.sensors;
using SecuritySystem.services.listeners;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class BuildingFactory
    {
        private Random random = new Random();

        public Building CreateBuilding(BuildingType type)
        {
            if (type == BuildingType.Commercial)
            {
                return CreateCommercialBuilding(1);
            }
            else if (type == BuildingType.Residential)
            {
                return CreateResidentialBuilding(1);
            }
            else
            {
                return CreateOffice(1);
            }
        }

        private Building CreateResidentialBuilding(long buildingId)
        {
            Building building = new Building(buildingId, "", "", new List<EventListener>() {
                    new FireEventListener(1),
                    new FloodEventListener(2),
                    new InvasionEventListener(3),
                    new JehovaWitnessessEventListener(4)
               });

            building.Floors = new List<Floor>();
            int i = 0;
            Floor floor = new Floor();
            floor.Id = i;
            floor.Rooms = new List<Room>();

            floor.Rooms.Add(CreateLivingRoom(building.Id, i, 0, 1, 20));
            floor.Rooms.Add(CreateKitchen(building.Id, i, 1, 1, 20));
            floor.Rooms.Add(CreateHall(building.Id, i, 2, 1, 20));
            floor.Rooms.Add(CreateBathroom(building.Id, i, 3, 1, 20));
            i++;

            building.Floors.Add(floor);

            long numberOfFloors = random.Next(3);
            for (int k = 0; k < numberOfFloors; k++)
            {
                Floor floor2 = new Floor();
                floor2.Id = i;
                floor2.Rooms = new List<Room>();

                floor2.Rooms.Add(CreateLivingRoom(building.Id, i, 0, 1, 20));
                floor2.Rooms.Add(CreateLivingRoom(building.Id, i, 1, 1, 20));
                floor2.Rooms.Add(CreateHall(building.Id, i, 2, 1, 20));
                floor2.Rooms.Add(CreateBathroom(building.Id, i, 3, 1, 20));
                i++;

                building.Floors.Add(floor2);
            }

            Floor floor3 = new Floor();
            floor3.Id = i;
            floor3.Rooms = new List<Room>();
            floor3.Rooms.Add(CreateAttic(building.Id, i, 0, 1, 20));
            building.Floors.Add(floor3);

            return building;
        }

        private Building CreateOffice(long buildingId)
        {
            Building building = new Building(buildingId, "", "", new List<EventListener>() {
                    new FireEventListener(1),
                    new FloodEventListener(2),
                    new InvasionEventListener(3),
                    new JehovaWitnessessEventListener(4)
               });

            long numberOfFloors = random.Next(1, 6);
            building.Floors = new List<Floor>();
            for (int i = 0; i < numberOfFloors; i++)
            {
                Floor floor = new Floor();
                floor.Id = i;
                floor.Rooms = new List<Room>();

                long numberOfRooms = random.Next(1, 5);

                for (int j = 0; j < numberOfRooms; j++)
                {
                    Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
                    List<Hole> holes = new List<Hole>();
                    long id = 1;

                    Window window1 = new Window(id++, 1, false);
                    Window window2 = new Window(id++, 1, false);
                    Door door = new Door(id++, 1, false);
                    holes.Add(window1);
                    holes.Add(window2);
                    holes.Add(door);

                    Room room = new Room(j, 1, holes, 20, $"room {i} {j}");

                    State state = new State(building.Id, i, j);

                    sensors.Add(window1, new List<Sensor>() { new InvasionSensor(id++, state) });
                    sensors.Add(window2, new List<Sensor>() { new InvasionSensor(id++, state) });
                    sensors.Add(door, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });

                    List<Sensor> roomSensors = new List<Sensor>();
                    long numberOfSensors = random.Next(1, 5);
                    for (int x = 0; x < numberOfSensors; x++)
                    {
                        roomSensors.Add(new FireSensor(id++, state));
                    }
                    numberOfSensors = random.Next(1, 5);
                    for (int x = 0; x < numberOfSensors; x++)
                    {
                        roomSensors.Add(new FloodSensor(id++, state));
                    }

                    sensors.Add(room, roomSensors);

                    room.Sensors = sensors;
                    floor.Rooms.Add(room);
                }
                building.Floors.Add(floor);
            }
            return building;
        }

        private Building CreateCommercialBuilding(long buildingId)
        {
            Building building = new Building(buildingId, "", "", new List<EventListener>() {
                    new FireEventListener(1),
                    new FloodEventListener(2),
                    new InvasionEventListener(3),
                    new JehovaWitnessessEventListener(4)
               });

            long numberOfFloors = random.Next(1, 3);
            building.Floors = new List<Floor>();
            for (int i = 0; i < numberOfFloors; i++)
            {
                Floor floor = new Floor();
                floor.Id = i;
                floor.Rooms = new List<Room>();

                long numberOfRooms = random.Next(1, 5);

                for (int j = 0; j < numberOfRooms; j++)
                {
                    Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
                    List<Hole> holes = new List<Hole>();
                    long id = 1;

                    Window window1 = new Window(id++, 1, false);
                    Window window2 = new Window(id++, 1, false);
                    Door door = new Door(id++, 1, false);
                    holes.Add(window1);
                    holes.Add(window2);
                    holes.Add(door);

                    Room room = new Room(j, 1, holes, 20, $"room {i} {j}");

                    State state = new State(building.Id, i, j);

                    sensors.Add(window1, new List<Sensor>() { new InvasionSensor(id++, state) });
                    sensors.Add(window2, new List<Sensor>() { new InvasionSensor(id++, state) });
                    sensors.Add(door, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
                    
                    List<Sensor> roomSensors = new List<Sensor>();
                    long numberOfSensors = random.Next(1, 5);
                    for (int x = 0; x < numberOfSensors; x++)
                    {
                        roomSensors.Add(new FireSensor(id++, state));
                    }
                    numberOfSensors = random.Next(1, 5);
                    for (int x = 0; x < numberOfSensors; x++)
                    {
                        roomSensors.Add(new FloodSensor(id++, state));
                    }
                    sensors.Add(room, roomSensors);

                    room.Sensors = sensors;
                    floor.Rooms.Add(room);
                }
                building.Floors.Add(floor);
            }
            return building;
        }

        private Room CreateLivingRoom(long buildingId, long floorId, long roomId, int safetyLevel, double square)
        {
            Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
            List<Hole> holes = new List<Hole>();
            long id = 1;

            Window window1 = new Window(id++, 1, false);
            Window window2 = new Window(id++, 1, false);
            Door door1 = new Door(id++, 1, false);
            Door door2 = new Door(id++, 1, false);
            holes.Add(window1);
            holes.Add(window2);
            holes.Add(door1);
            holes.Add(door2);

            Room room = new Room(roomId, 1, holes, 20, "living room");

            State state = new State(buildingId, floorId, roomId);

            sensors.Add(window1, new List<Sensor>() { new InvasionSensor(id++, state) });
            sensors.Add(window2, new List<Sensor>() { new InvasionSensor(id++, state) });
            sensors.Add(door1, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            sensors.Add(door2, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            List<Sensor> roomSensors = new List<Sensor>();
            long numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FireSensor(id++, state));
            }
            numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FloodSensor(id++, state));
            }
            sensors.Add(room, roomSensors);

            room.Sensors = sensors;
            return room;
        }

        private Room CreateKitchen(long buildingId, long floorId, long roomId, int safetyLevel, double square)
        {
            Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
            List<Hole> holes = new List<Hole>();
            long id = 1;

            Window window1 = new Window(id++, 1, false);
            Door door1 = new Door(id++, 1, false);
            holes.Add(window1);
            holes.Add(door1);

            Room room = new Room(roomId, 1, holes, 20, "kitchen");

            State state = new State(buildingId, floorId, roomId);

            sensors.Add(window1, new List<Sensor>() { new InvasionSensor(id++, state) });
            sensors.Add(door1, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            List<Sensor> roomSensors = new List<Sensor>();
            long numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FireSensor(id++, state));
            }
            numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FloodSensor(id++, state));
            }
            sensors.Add(room, roomSensors);

            room.Sensors = sensors;
            return room;
        }

        private Room CreateHall(long buildingId, long floorId, long roomId, int safetyLevel, double square)
        {
            Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
            List<Hole> holes = new List<Hole>();
            long id = 1;

            Door door1 = new Door(id++, 1, false);
            Door door2 = new Door(id++, 1, false);
            Door door3 = new Door(id++, 1, false);
            Door door4 = new Door(id++, 1, false);

            holes.Add(door1);
            holes.Add(door2);
            holes.Add(door3);
            holes.Add(door4);

            Room room = new Room(roomId, 1, holes, 20, "hall");

            State state = new State(buildingId, floorId, roomId);

            sensors.Add(door1, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            sensors.Add(door2, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            sensors.Add(door3, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            sensors.Add(door4, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            List<Sensor> roomSensors = new List<Sensor>();
            long numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FireSensor(id++, state));
            }
            numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FloodSensor(id++, state));
            }
            sensors.Add(room, roomSensors);

            room.Sensors = sensors;
            return room;
        }

        private Room CreateWareHouse(int safetyLevel, double square, State state)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(1, safetyLevel, false),

                new Window(2, safetyLevel, false),

                new Window(3, safetyLevel, false),

                new Window(4, safetyLevel, false)
            };

            Room room = new Room(1488, safetyLevel, holes, square, "warehouse");
            List<Sensor> sensorsList = new List<Sensor>();
            sensorsList.Add(new FloodSensor(4, state));
            Dictionary<ISensorOwner, List<Sensor>> dictionary = new Dictionary<ISensorOwner, List<Sensor>>();
            dictionary.Add(holes[0], sensorsList);
            room.Sensors = dictionary;
            return room;
        }

        private Room CreateBathroom(long buildingId, long floorId, long roomId, int safetyLevel, double square)
        {
            Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
            List<Hole> holes = new List<Hole>();
            long id = 1;

            Door door1 = new Door(id++, 1, false);
            Door door2 = new Door(id++, 1, false);
            holes.Add(door1);
            holes.Add(door2);

            Room room = new Room(roomId, 1, holes, 20, "bathroom");

            State state = new State(buildingId, floorId, roomId);

            sensors.Add(door1, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            sensors.Add(door2, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });
            List<Sensor> roomSensors = new List<Sensor>();
            long numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FireSensor(id++, state));
            }
            numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FloodSensor(id++, state));
            }
            sensors.Add(room, roomSensors);

            room.Sensors = sensors;
            return room;
        }

        private Room CreateAttic(long buildingId, long floorId, long roomId, int safetyLevel, double square)
        {
            Dictionary<ISensorOwner, List<Sensor>> sensors = new Dictionary<ISensorOwner, List<Sensor>>();
            List<Hole> holes = new List<Hole>();
            long id = 1;

            Window window1 = new Window(id++, 1, false);
            Window window2 = new Window(id++, 1, false);
            Window window3 = new Window(id++, 1, false);
            Door door1 = new Door(id++, 1, false);
            holes.Add(window1);
            holes.Add(window2);
            holes.Add(window3);
            holes.Add(door1);

            Room room = new Room(roomId, 1, holes, 20, "attic");

            State state = new State(buildingId, floorId, roomId);

            sensors.Add(window1, new List<Sensor>() { new InvasionSensor(id++, state) });
            sensors.Add(window2, new List<Sensor>() { new InvasionSensor(id++, state) });
            sensors.Add(window3, new List<Sensor>() { new InvasionSensor(id++, state) });
            sensors.Add(door1, new List<Sensor>() {
                        new InvasionSensor(id++, state),
                        new JehovaWitnessesSensor(id++, state)
                    });

            List<Sensor> roomSensors = new List<Sensor>();
            long numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FireSensor(id++, state));
            }
            numberOfSensors = random.Next(1, 5);
            for (int x = 0; x < numberOfSensors; x++)
            {
                roomSensors.Add(new FloodSensor(id++, state));
            }
            sensors.Add(room, roomSensors);

            room.Sensors = sensors;
            return room;
        }

        private Room CreateOpenSpace(int safetyLevel, double square, State state)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(1, safetyLevel, false),
                new Window(2, safetyLevel, false),
                new Window(3, safetyLevel, false),
                new Window(4, safetyLevel, false),
                new Window(5, safetyLevel, false),
                new Window(6, safetyLevel, false),
                new Window(7, safetyLevel, false)
            };

            Room room = new Room(1999, safetyLevel, holes, square, "openspace");
            List<Sensor> sensorsList = new List<Sensor>();
            sensorsList.Add(new JehovaWitnessesSensor(228, state));
            Dictionary<ISensorOwner, List<Sensor>> dictionary = new Dictionary<ISensorOwner, List<Sensor>>();
            dictionary.Add(holes[0], sensorsList);
            room.Sensors = dictionary;
            return room;
        }
    }
}

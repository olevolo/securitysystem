﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Door : Hole
    {
        public Door(long id, int safetyLevel, bool isOpen) : base(id, safetyLevel, isOpen) { }
    }
}


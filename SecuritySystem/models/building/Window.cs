﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Window : Hole
    {
        public Window(long id, int safetyLevel, bool isOpen) : base(id, safetyLevel, isOpen) { }
    }
}

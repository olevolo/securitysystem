﻿using SecuritySystem.services.listeners;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Building
    {
        public long Id { get; set; }

        public List<Floor> Floors { get; set; }

        public List<EventListener> Activators { get; set; }

        public string Address { get; set; }

        public string PostIndex { get; set; }

        public Building(long id) {
            Id = id;
        }

        public Building (long id, string address, string postIndex)
        {
            Id = id;
            Address = address;
            PostIndex = postIndex;
            Activators = new List<EventListener>();
        }

        public Building(long id, string address, string postIndex, List<EventListener> list) {
            Id = id;
            Address = address;
            PostIndex = postIndex;
            Activators = list;
        }

        
    }
}

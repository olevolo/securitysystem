﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class State
    {
        public long BuildingId { get; set; }
        public long FloorId { get; set; }
        public long RoomId { get; set; }

        public State(long buildingId, long floorId, long roomId) {
            BuildingId = buildingId;
            FloorId = floorId;
            RoomId = roomId;
        }
        
        public override bool Equals(object obj)
        {
            if ((obj == null) && (obj.GetType().Equals(this.GetType()))) {
                return false;
            }
            State s = (State)obj;
            return s.BuildingId == this.BuildingId && this.FloorId == s.FloorId && this.RoomId == s.RoomId;
        }

        public override string ToString()
        {
            return "building:" + BuildingId + " |floor:" + FloorId + " |room:" + RoomId;
        }

        public string ForTextBox()
        {
            return "building:" + BuildingId + "  floor:" + (FloorId + 1) + "  room:" + (RoomId + 1);
        }
    }
}

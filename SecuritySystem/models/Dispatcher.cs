﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class Dispatcher
    {
        private Queue<Message> queue;
        private static Dispatcher dispatcher;
        private int cycle = 0;

        public static Dispatcher getInstance()
        {
            if (dispatcher == null)
                dispatcher = new Dispatcher();
            return dispatcher;
        }

        private Dispatcher()
        {
            this.queue = new Queue<Message>();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void dispatchMessage(Message message)
        {
            this.queue.Enqueue(message);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Message getFirstMessage()
        {
            try
            {
                return this.queue.First();
            }
            catch (Exception e)
            {
                return new Message(new State(0, 0, 0), EventType.JEHOVAH_WITNESSES, "0");
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool ContainsElements()
        {
            return queue.Count > 0;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Message getFirstMessageAndRemove()
        {
            //try
            //{
                Message message = this.queue.Dequeue();
                return message;
            //}
            //catch (Exception e)
            //{
            //    return new Message(new State(0, 0, 0), EventType.JEHOVAH_WITNESSES, "0");
            //}
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Message getLastMessage()
        {
            return this.queue.Last();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void EraseQueue()
        {
            cycle++;
            if (cycle == 4) {
                getFirstMessageAndRemove();
                cycle = 0;
            }
        }

    }
}
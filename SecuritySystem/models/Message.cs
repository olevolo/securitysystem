﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class Message
    {
        public State State { get; set; }
        public EventType Type { get; set; }
        public string Time { get; set; }
        public long Id { get; set; }

        private static int AUTO_ID = 0;

        public Message(State state, EventType type, string time)
        {
            this.Id = AUTO_ID++;
            this.State = state;
            this.Type = type;
            this.Time = time;
        }

        public override string ToString()
        {
            return "Message" + Id.ToString() + ": " + State.ToString() + " |type:" + Type + "|time: " + Time;
        }

    }
}

﻿using MaterialDesignThemes.Wpf;
using SecuritySystem.models;
using SecuritySystem.models.building;
using SecuritySystem.models.sensors;
using SecuritySystem.services;
using SecuritySystem.services.listeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Threading;
using System.Threading;

namespace SecuritySystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class BuildingWindow : System.Windows.Window
    {
        Building building;
        Room currentRoom = null;
        int floorNumber;

        public BuildingWindow(BuildingType buildingType)
        {
            InitializeComponent();
            BuildingFactory buildingFactory = new BuildingFactory();
            building = buildingFactory.CreateBuilding(buildingType);

            EventGenerator eventGenerator = EventGenerator.getInstance();
            eventGenerator.SetProperties(building);
            eventGenerator.Run();

            
            floorsBox.SelectedIndex = 0;
            for (int floor = 0; floor < building.Floors.Count; floor++)
            {
                floorsBox.Items.Add("Floor " + (building.Floors[floor].Id + 1).ToString());
            }

        }

        static private Building CreateBuilding()
        {
            Building building = new Building(1, "14", "88",
               new List<EventListener>() {
                    new FireEventListener(1),
                    new FloodEventListener(2),
                    new InvasionEventListener(3),
                    new JehovaWitnessessEventListener(4)
               });
            List<Hole> holes = new List<Hole>() { new models.building.Window(1, 0, false) };
            Dictionary<ISensorOwner, List<Sensor>> dic = new Dictionary<ISensorOwner, List<Sensor>>();
            State state = new State(1, 0, 0);
            Room room1 = new Room(0, 0, holes, 10, "toi-toi");
            dic.Add(holes[0], new List<Sensor>() { new InvasionSensor(1, state) });
            dic.Add(room1, new List<Sensor>() { new FireSensor(2, state), new FloodSensor(3, state), new InvasionSensor(4, state), new JehovaWitnessesSensor(5, state) });
            room1.Sensors = dic;
            List<Room> rooms = new List<Room>() { room1 };
            building.Floors = new List<Floor>() { new Floor(state.FloorId, rooms) };
            return building;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (roomsPanel.Children.Count > 1)
            {
                roomsPanel.Children.RemoveRange(1, roomsPanel.Children.Count - 1);
            }
            string floor = (sender as ComboBox).SelectedItem as string;
            floorNumber = Int16.Parse(floor.Substring(6)) - 1;
            for (int room = 0; room < building.Floors[floorNumber].Rooms.Count; room++)
            {
                System.Windows.Controls.Button button = new Button();
                Room currentRoom = building.Floors[floorNumber].Rooms[room];
                button.Content = currentRoom.Type.ToString();
                button.Click += (sender2, e2) => changeSelectedRoom(sender2, e2, currentRoom);
                button.Height = 60;
                button.Padding = new Thickness(11, 0, 0, 0);
                button.FontWeight = FontWeights.DemiBold;
                button.FontFamily = new FontFamily("Aharoni");
                button.FontSize = 20;
                button.Background = new SolidColorBrush(Color.FromRgb(77, 80, 97));
                button.Foreground = Brushes.White;
                button.VerticalAlignment = VerticalAlignment.Center;
                button.HorizontalContentAlignment = HorizontalAlignment.Left;
                button.BorderThickness = new Thickness(0);
                button.Name = $"room{room}";
                roomsPanel.Children.Add(button);
            }
            dispatcher = new System.Windows.Threading.DispatcherTimer();
            dispatcher.Tick += new EventHandler(roomRepainting);
            dispatcher.Interval = new TimeSpan(0, 0, 1);
            dispatcher.Start();
        }

        private void roomRepainting(object sender, EventArgs e)
        {
            for (int room = 0; room < building.Floors[floorNumber].Rooms.Count; room++)
            {
                bool inDanger = this.building.Floors[floorNumber].Rooms[room].Sensors.Values
                    .Select(val => val.Where(sens => sens.Active == false).ToList().Count > 0)
                    .Where(item => item == true).ToList().Count > 0;

                Button btn = (Button)roomsPanel.Children[1 + room];
                btn.Background = inDanger ? new SolidColorBrush(Color.FromRgb(225, 127, 127)) : new SolidColorBrush(Color.FromRgb(77, 80, 97));
                btn.BorderThickness = new Thickness(0);
                if (this.currentRoom != null && btn.Content.ToString() == this.currentRoom.Type)
                {
                    //btn.Background = Brushes.Yellow;
                    btn.BorderBrush = Brushes.White;
                    btn.BorderThickness = new Thickness(2);
                }
            }
            List<Log> logs = Journal.getInstance().lastActivator();

            string str = "";
            foreach (var lastEvent in logs)
            {
                str += ("Подія трапилася ! \n" + "Поверх: " + (lastEvent.State.FloorId + 1).ToString() + "\nКімната: " + (lastEvent.State.RoomId + 1).ToString() + "\nПодія: " + lastEvent.EvType.ToString() + "\nДопомога: " + lastEvent.RespType.ToString() + "\n\n");
            }
            runningInfo.Text = (logs.FirstOrDefault() != null) ? str: "\n\n\n\nСистема під контролем";
            system_cond.Fill = (logs.FirstOrDefault() != null) ? new SolidColorBrush(Color.FromRgb(225, 127, 127)) : new SolidColorBrush(Color.FromRgb(91, 156, 100));
        }
        public DispatcherTimer dispatcher;
        private void changeSelectedRoom(object sender, RoutedEventArgs e, Room room)
        {
            this.currentRoom = room;
            displaySelectedRoom(sender, e);
            dispatcher.Tick += new EventHandler(displaySelectedRoom);
            
        }

        private void displaySelectedRoom(object sender, EventArgs e)
        {
            
            for (int i = 1; i < 5; i++)
            {
                List<Grid> forHide = new List<Grid>();
                forHide.Add((Grid)this.FindName($"fire{i}"));
                forHide.Add((Grid)this.FindName($"water{i}"));
                forHide.Add((Grid)this.FindName($"robber{i}"));
                forHide.Add((Grid)this.FindName($"witness{i}"));
                forHide.ForEach(item => item.Visibility = Visibility.Collapsed);
            }
            //system_cond.Fill = System.Windows.Media.Brushes.White;


            int robberIdx = 1, waterIdx = 1, fireIdx = 1, witnessIdx = 1;
            foreach (var sensor in this.currentRoom.Sensors)
            {
                string owner = sensor.Key.GetType().Name.ToString();
                foreach (var currentHoleSensor in sensor.Value)
                {
                    switch (currentHoleSensor.GetType().Name.ToString())
                    {
                        case "FireSensor":
                            paintPicture($"fire{fireIdx++}", currentHoleSensor, owner);
                            break;
                        case "FloodSensor":
                            paintPicture($"water{waterIdx++}", currentHoleSensor, owner);
                            break;
                        case "InvasionSensor":
                            paintPicture($"robber{robberIdx++}", currentHoleSensor, owner);
                            break;
                        case "JehovaWitnessesSensor":
                            paintPicture($"witness{witnessIdx++}", currentHoleSensor, owner);
                            break;
                        default:
                            //system_cond.Fill = new SolidColorBrush(Color.FromRgb(225, 127, 127));
                            break;
                    }
                }
            }

            togglersPainting();
        }
        public void paintPicture(string name, Sensor currentSensor, string labelContent)
        {
            Grid sensor = (Grid)this.FindName(name);
            sensor.Visibility = Visibility.Visible;
            PackIcon picture = (PackIcon)sensor.Children[1];
            Label label = (Label)sensor.Children[0];
            label.Content = labelContent;

            Binding bnd = new Binding();
            bnd.Source = currentSensor.Active ? System.Windows.Media.Brushes.White : new SolidColorBrush(Color.FromRgb(225, 127, 127));
            //bnd.Path = new PropertyPath("Active");
            //bnd.Mode = BindingMode.TwoWay;
            bnd.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            BindingOperations.SetBinding(picture, PackIcon.ForegroundProperty, bnd);

        }

        bool checkerChildrens(string type, int i)
        {
            
            if (((PackIcon)((Grid)this.FindName(type + i)).Children[1]).Foreground.ToString() == "#FFE17F7F")
            {
                return true;
            }
            return false;
        }

        void repaintingColor(PackIcon icn, bool toggler, RotateTransform rotator)
        {
            if (toggler)
            {
                icn.Foreground = new SolidColorBrush(Color.FromRgb(225, 127, 127));
                icn.RenderTransform = rotator;
            }
            else
            {
                icn.Foreground = Brushes.White;
                icn.RenderTransform = null;
            }
        }

        public void togglersPainting()
        {
            bool fireTogglerVar = false;
            bool waterTogglerVar = false;
            bool robberTogglerVar = false;
            bool witnessTogglerVar = false;
            for (int i = 1; i < 5; i++)
            {

                if (checkerChildrens("fire", i))
                {
                    fireTogglerVar = true;
                }
                if (checkerChildrens("water", i))
                {
                    waterTogglerVar = true;
                }
                if (checkerChildrens("robber", i))
                {
                    robberTogglerVar = true;
                }
                if (checkerChildrens("witness", i))
                {
                    witnessTogglerVar = true;
                }

            }
            RotateTransform rotator = new RotateTransform(180, 35, 35);

            repaintingColor(fireToggler, fireTogglerVar, rotator);
            repaintingColor(waterToggler, waterTogglerVar, rotator);
            repaintingColor(robberToggler, robberTogglerVar, rotator);
            repaintingColor(witnessToggler, witnessTogglerVar, rotator);

            if (robberTogglerVar || waterTogglerVar || robberTogglerVar || witnessTogglerVar)
            {
                room_ok_text.Content = "небезпечно";
                room_ok.Fill = new SolidColorBrush(Color.FromRgb(225, 127, 127));
            }
            else
            {
                room_ok_text.Content = "безпечно";
                room_ok.Fill = new SolidColorBrush(Color.FromRgb(91, 156, 100));
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Close();
        }

    }
}

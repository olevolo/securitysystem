﻿using SecuritySystem.models;
using SecuritySystem.models.building;
using SecuritySystem.models.sensors;
using SecuritySystem.services;
using SecuritySystem.services.listeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SecuritySystem.ConsoleManager;
namespace SecuritySystem
{
    class Startup
    {

        [STAThreadAttribute]
        public static int Main(string[] args)
        {
            // run console
            ConsoleManager.Show();

            BuildingFactory buildingFactory = new BuildingFactory();
            //Building building = buildingFactory.CreateBuilding(BuildingType.Residential, "LNU", "999");
            Building building = buildingFactory.CreateBuilding(BuildingType.Residential);
            //Building building = CreateBuilding();

            EventGenerator eventGenerator = EventGenerator.getInstance();
            eventGenerator.SetProperties(building);
            eventGenerator.Run();
           
            Console.ReadKey();

            return 0;
        }

        static private Building CreateBuilding()
        {
            Building building = new Building(1, "14", "88",
               new List<EventListener>() {
                    new FireEventListener(1),
                    new FloodEventListener(2),
                    new InvasionEventListener(3),
                    new JehovaWitnessessEventListener(4)
               });
            List<Hole> holes = new List<Hole>() { new Window(1, 0, false) };
            Dictionary<ISensorOwner, List<Sensor>> dic = new Dictionary<ISensorOwner, List<Sensor>>();
            State state = new State(1, 0, 0);
            Room room1 = new Room(0, 0, holes, 10, "toi-toi");
            dic.Add(holes[0], new List<Sensor>() { new InvasionSensor(1, state) });
            dic.Add(room1, new List<Sensor>() { new FireSensor(2, state), new FloodSensor(3, state), new InvasionSensor(4, state), new JehovaWitnessesSensor(5, state) });
            room1.Sensors = dic;
            List<Room> rooms = new List<Room>() { room1 };
            building.Floors = new List<Floor>() { new Floor(state.FloorId, rooms) };
            return building;
        }
    }
}
